# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :privacy_vault,
  ecto_repos: [PrivacyVault.Repo]

# Configures the endpoint
config :privacy_vault, PrivacyVaultWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pWj6/vIdyM4xuG8SKcL9wk4+Yem5bqn+rG+eKX8bOFWhfQf41vJ7Q8ov9vAe/H50",
  render_errors: [view: PrivacyVaultWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PrivacyVault.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Add slim template engine
config :phoenix, :template_engines,
  slim: PhoenixSlime.Engine,
  slime: PhoenixSlime.Engine

# Configures Torch
config :torch,
  otp_app: :privacy_vault,
  template_format: "eex"

# Configures generators
config :privacy_vault, :generators, binary_id: true

# Configures arc
config :arc,
  storage: Arc.Storage.Local

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
