defmodule PrivacyVaultWeb.Router do
  use PrivacyVaultWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/admin", PrivacyVaultWeb.Admin, as: :admin do
    pipe_through :browser

    get "/", PageController, :index

    resources "/requests", RequestController
    resources "/centers", CenterController
  end

  scope "/center", PrivacyVaultWeb.Center, as: :center do
    pipe_through :browser

    resources "/", CenterController, only: [:show]
    get "/requests/:id", RequestController, :edit
    post "/requests/:id", RequestController, :update
    post "/requests/:id/upload", RequestController, :upload
  end

  scope "/", PrivacyVaultWeb do
    pipe_through :browser

    get "/", PageController, :index

    resources "/request", RequestController, only: [:show, :new, :create]
  end
end
