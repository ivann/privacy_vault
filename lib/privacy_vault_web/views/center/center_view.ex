defmodule PrivacyVaultWeb.Center.CenterView do
  use PrivacyVaultWeb, :view

  def center_request_links(conn, center) do
    links =
      Enum.map(center.center_requests, fn request ->
        link(request.id, to: Routes.center_request_path(conn, :edit, request.id))
      end)

    content_tag(:span, Enum.intersperse(links, ", "))
  end
end
