defmodule PrivacyVaultWeb.RequestView do
  use PrivacyVaultWeb, :view

  def center_names(centers) do
    centers
    |> Enum.map(& &1.name)
    |> Enum.join(", ")
  end
end
