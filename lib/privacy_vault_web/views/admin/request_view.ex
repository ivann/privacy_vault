defmodule PrivacyVaultWeb.Admin.RequestView do
  use PrivacyVaultWeb, :view

  import Torch.TableView
  import Torch.FilterView

  def center_links(conn, centers) do
    links =
      Enum.map(centers, fn center ->
        link(center.name, to: Routes.admin_center_path(conn, :show, center))
      end)

    content_tag(:span, Enum.intersperse(links, ", "))
  end

  def selected_centers(nil), do: []

  def selected_centers(request) do
    Enum.map(request.centers, & &1.id)
  end
end
