defmodule PrivacyVaultWeb.Admin.CenterView do
  use PrivacyVaultWeb, :view

  import Torch.TableView
  import Torch.FilterView
end
