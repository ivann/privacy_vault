defmodule PrivacyVaultWeb.Admin.PageController do
  use PrivacyVaultWeb, :controller

  plug(:put_layout, {PrivacyVaultWeb.LayoutView, "torch.html"})

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
