defmodule PrivacyVaultWeb.Admin.RequestController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Centers
  alias PrivacyVault.Vault
  alias PrivacyVault.Vault.Request

  plug(:put_layout, {PrivacyVaultWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Vault.paginate_requests(params) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)

      error ->
        conn
        |> put_flash(:error, "There was an error rendering Requests. #{inspect(error)}")
        |> redirect(to: Routes.admin_request_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Vault.change_request(%Request{centers: []})
    centers_select = Centers.select_centers_list()

    render(conn, "new.html",
      request: nil,
      centers_select: centers_select,
      changeset: changeset
    )
  end

  def create(conn, %{"request" => request_params}) do
    case Vault.create_request(request_params) do
      {:ok, request} ->
        conn
        |> put_flash(:info, "Request created successfully.")
        |> redirect(to: Routes.admin_request_path(conn, :show, request))

      {:error, %Ecto.Changeset{} = changeset} ->
        centers_select = Centers.select_centers_list()

        render(conn, "new.html",
          request: nil,
          centers_select: centers_select,
          changeset: changeset
        )
    end
  end

  def show(conn, %{"id" => id}) do
    request = Vault.get_request_with_centers!(id)
    render(conn, "show.html", request: request)
  end

  def edit(conn, %{"id" => id}) do
    request = Vault.get_request_with_centers!(id)
    centers_select = Centers.select_centers_list()
    changeset = Vault.change_request(request)

    render(conn, "edit.html",
      request: request,
      centers_select: centers_select,
      changeset: changeset
    )
  end

  def update(conn, %{"id" => id, "request" => request_params}) do
    request = Vault.get_request_with_centers!(id)

    case Vault.update_request(request, request_params) do
      {:ok, request} ->
        conn
        |> put_flash(:info, "Request updated successfully.")
        |> redirect(to: Routes.admin_request_path(conn, :show, request))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          request: request,
          centers_select: Centers.select_centers_list(),
          changeset: changeset
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    request = Vault.get_request!(id)
    {:ok, _request} = Vault.delete_request(request)

    conn
    |> put_flash(:info, "Request deleted successfully.")
    |> redirect(to: Routes.admin_request_path(conn, :index))
  end
end
