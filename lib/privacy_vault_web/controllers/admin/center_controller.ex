defmodule PrivacyVaultWeb.Admin.CenterController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Centers
  alias PrivacyVault.Centers.Center

  plug(:put_layout, {PrivacyVaultWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Centers.paginate_centers(params) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)

      error ->
        conn
        |> put_flash(:error, "There was an error rendering Centers. #{inspect(error)}")
        |> redirect(to: Routes.admin_center_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Centers.change_center(%Center{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"center" => center_params}) do
    case Centers.create_center(center_params) do
      {:ok, center} ->
        conn
        |> put_flash(:info, "Center created successfully.")
        |> redirect(to: Routes.admin_center_path(conn, :show, center))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    center = Centers.get_center!(id)
    render(conn, "show.html", center: center)
  end

  def edit(conn, %{"id" => id}) do
    center = Centers.get_center!(id)
    changeset = Centers.change_center(center)
    render(conn, "edit.html", center: center, changeset: changeset)
  end

  def update(conn, %{"id" => id, "center" => center_params}) do
    center = Centers.get_center!(id)

    case Centers.update_center(center, center_params) do
      {:ok, center} ->
        conn
        |> put_flash(:info, "Center updated successfully.")
        |> redirect(to: Routes.admin_center_path(conn, :show, center))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", center: center, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    center = Centers.get_center!(id)
    {:ok, _center} = Centers.delete_center(center)

    conn
    |> put_flash(:info, "Center deleted successfully.")
    |> redirect(to: Routes.admin_center_path(conn, :index))
  end
end
