defmodule PrivacyVaultWeb.Center.CenterController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Centers

  def show(conn, %{"id" => id}) do
    center = Centers.get_center_with_requests!(id)
    render(conn, "show.html", center: center)
  end
end
