defmodule PrivacyVaultWeb.Center.RequestController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Vault

  def edit(conn, %{"id" => id}) do
    center_request = Vault.get_center_request!(id)
    public_key = Vault.get_request!(center_request.request_id).key
    changeset = Vault.change_center_request(center_request)
    csrf_token = get_csrf_token()

    render(conn, "edit.html",
      center_request: center_request,
      changeset: changeset,
      public_key: public_key,
      csrf_token: csrf_token
    )
  end

  def update(conn, %{"id" => id, "center_request" => center_request_params}) do
    center_request = Vault.get_center_request!(id)

    if Map.get(center_request_params, "complete") == "true" do
      Map.put(center_request_params, "completed_at", DateTime.utc_now())
    end

    case Vault.update_center_request(center_request, center_request_params) do
      {:ok, _center_request} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(200, "")

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(500, "")
    end
  end

  def upload(conn, %{"id" => id, "file" => file_params}) do
    case Vault.create_private_data(id, file_params) do
      {:ok, _private_data} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(200, "")

      {:error, %Ecto.Changeset{} = _changeset} ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(500, "")
    end
  end
end
