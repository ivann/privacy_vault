defmodule PrivacyVaultWeb.RequestController do
  use PrivacyVaultWeb, :controller

  alias PrivacyVault.Centers
  alias PrivacyVault.Vault
  alias PrivacyVault.Vault.Request

  def new(conn, _params) do
    changeset = Vault.change_request(%Request{centers: []})
    centers_select = Centers.select_centers_list()
    render(conn, "new.html", centers_select: centers_select, changeset: changeset)
  end

  def create(conn, %{"request" => request_params}) do
    case Vault.create_request(request_params) do
      {:ok, request} ->
        conn
        |> put_flash(:info, "Request created successfully.")
        |> redirect(to: Routes.request_path(conn, :show, request))

      {:error, %Ecto.Changeset{} = changeset} ->
        centers_select = Centers.select_centers_list()
        render(conn, "new.html", centers_select: centers_select, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    request = Vault.get_request_with_centers!(id)
    render(conn, "show.html", request: request)
  end
end
