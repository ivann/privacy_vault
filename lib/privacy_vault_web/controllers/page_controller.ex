defmodule PrivacyVaultWeb.PageController do
  use PrivacyVaultWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
