defmodule PrivacyVault.Centers.Center do
  use Ecto.Schema
  import Ecto.Changeset

  alias PrivacyVault.Vault.Request
  alias PrivacyVault.Vault.CenterRequest

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "centers" do
    field :country, :string
    field :email, :string
    field :name, :string

    many_to_many :requests, Request, join_through: CenterRequest
    has_many :center_requests, CenterRequest

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(center, attrs) do
    center
    |> cast(attrs, [:name, :email, :country])
    |> validate_required([:name, :email, :country])
  end
end
