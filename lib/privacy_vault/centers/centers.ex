defmodule PrivacyVault.Centers do
  @moduledoc """
  The Centers context.
  """

  import Ecto.Query, warn: false
  alias PrivacyVault.Repo

  import Torch.Helpers, only: [sort: 1, paginate: 4]
  import Filtrex.Type.Config

  alias PrivacyVault.Centers.Center

  @pagination [page_size: 15]
  @pagination_distance 5

  @doc """
  Paginate the list of centers using filtrex
  filters.

  ## Examples

      iex> list_centers(%{})
      %{centers: [%Center{}], ...}
  """
  @spec paginate_centers(map) :: {:ok, map} | {:error, any}
  def paginate_centers(params \\ %{}) do
    params =
      params
      |> Map.put_new("sort_direction", "desc")
      |> Map.put_new("sort_field", "inserted_at")

    {:ok, sort_direction} = Map.fetch(params, "sort_direction")
    {:ok, sort_field} = Map.fetch(params, "sort_field")

    with {:ok, filter} <-
           Filtrex.parse_params(center_filter_config(:centers), params["center"] || %{}),
         %Scrivener.Page{} = page <- do_paginate_centers(filter, params) do
      {:ok,
       %{
         centers: page.entries,
         page_number: page.page_number,
         page_size: page.page_size,
         total_pages: page.total_pages,
         total_entries: page.total_entries,
         distance: @pagination_distance,
         sort_field: sort_field,
         sort_direction: sort_direction
       }}
    else
      {:error, error} -> {:error, error}
      error -> {:error, error}
    end
  end

  defp do_paginate_centers(filter, params) do
    Center
    |> Filtrex.query(filter)
    |> order_by(^sort(params))
    |> paginate(Repo, params, @pagination)
  end

  @doc """
  Returns the list of centers.

  ## Examples

      iex> list_centers()
      [%Center{}, ...]

  """
  def list_centers do
    Repo.all(Center)
  end

  @doc """
  Returns the map of centers names to their id's.
  """
  def select_centers_list do
    Repo.all(from(c in Center, select: {c.name, c.id})) |> Map.new()
  end

  @doc """
  Gets a single center.

  Raises `Ecto.NoResultsError` if the Center does not exist.

  ## Examples

      iex> get_center!(123)
      %Center{}

      iex> get_center!(456)
      ** (Ecto.NoResultsError)

  """
  def get_center!(id), do: Repo.get!(Center, id)

  @doc """
  Gets a single center with it's requests.
  """
  def get_center_with_requests!(id) do
    center = get_center!(id)
    Repo.preload(center, :center_requests)
  end

  @doc """
  Creates a center.

  ## Examples

      iex> create_center(%{field: value})
      {:ok, %Center{}}

      iex> create_center(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_center(attrs \\ %{}) do
    %Center{}
    |> Center.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a center.

  ## Examples

      iex> update_center(center, %{field: new_value})
      {:ok, %Center{}}

      iex> update_center(center, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_center(%Center{} = center, attrs) do
    center
    |> Center.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Center.

  ## Examples

      iex> delete_center(center)
      {:ok, %Center{}}

      iex> delete_center(center)
      {:error, %Ecto.Changeset{}}

  """
  def delete_center(%Center{} = center) do
    Repo.delete(center)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking center changes.

  ## Examples

      iex> change_center(center)
      %Ecto.Changeset{source: %Center{}}

  """
  def change_center(%Center{} = center) do
    Center.changeset(center, %{})
  end

  defp center_filter_config(:centers) do
    defconfig do
      text(:name)
      text(:email)
      text(:country)
    end
  end
end
