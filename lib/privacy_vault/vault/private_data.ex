defmodule PrivacyVault.Vault.PrivateData do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset

  alias PrivacyVault.PrivateFile
  alias PrivacyVault.Vault.CenterRequest

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "private_data" do
    field :file, PrivateFile.Type

    belongs_to :center_request, CenterRequest

    timestamps()
  end

  @doc false
  def changeset(private_data, attrs) do
    private_data
    |> cast(attrs, [:center_request_id])
    |> cast_attachments(attrs, [:file])
    |> assoc_constraint(:center_request)
    |> validate_required([:file])
  end
end
