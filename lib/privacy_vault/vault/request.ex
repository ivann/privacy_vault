defmodule PrivacyVault.Vault.Request do
  use Ecto.Schema
  import Ecto.Changeset

  alias PrivacyVault.Centers
  alias PrivacyVault.Centers.Center
  alias PrivacyVault.Vault.CenterRequest

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "requests" do
    field :completed_at, :utc_datetime
    field :email, :string
    field :info, :string
    field :key, :string

    many_to_many :centers, Center, join_through: CenterRequest, on_replace: :delete
    has_many :center_requests, CenterRequest

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(request, attrs) do
    request
    |> cast(attrs, [:email, :key, :info, :completed_at])
    |> put_assoc(:centers, parse_center_ids(attrs))
    |> validate_required([:email, :key, :info])
  end

  defp parse_center_ids(attrs) do
    (attrs["center_ids"] || attrs[:center_ids] || []) |> Enum.map(&Centers.get_center!/1)
  end
end
