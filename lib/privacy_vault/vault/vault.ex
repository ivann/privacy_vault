defmodule PrivacyVault.Vault do
  @moduledoc """
  The Vault context.
  """

  import Ecto.Query, warn: false
  alias PrivacyVault.Repo

  import Torch.Helpers, only: [sort: 1, paginate: 4]
  import Filtrex.Type.Config

  alias PrivacyVault.Vault.Request
  alias PrivacyVault.Vault.CenterRequest
  alias PrivacyVault.Vault.PrivateData

  @pagination [page_size: 15]
  @pagination_distance 5

  @doc """
  Paginate the list of requests using filtrex
  filters.

  ## Examples

      iex> list_requests(%{})
      %{requests: [%Request{}], ...}
  """
  @spec paginate_requests(map) :: {:ok, map} | {:error, any}
  def paginate_requests(params \\ %{}) do
    params =
      params
      |> Map.put_new("sort_direction", "desc")
      |> Map.put_new("sort_field", "inserted_at")

    {:ok, sort_direction} = Map.fetch(params, "sort_direction")
    {:ok, sort_field} = Map.fetch(params, "sort_field")

    with {:ok, filter} <-
           Filtrex.parse_params(request_filter_config(:requests), params["request"] || %{}),
         %Scrivener.Page{} = page <- do_paginate_requests(filter, params) do
      {:ok,
       %{
         requests: page.entries,
         page_number: page.page_number,
         page_size: page.page_size,
         total_pages: page.total_pages,
         total_entries: page.total_entries,
         distance: @pagination_distance,
         sort_field: sort_field,
         sort_direction: sort_direction
       }}
    else
      {:error, error} -> {:error, error}
      error -> {:error, error}
    end
  end

  defp do_paginate_requests(filter, params) do
    Request
    |> Filtrex.query(filter)
    |> order_by(^sort(params))
    |> preload(:centers)
    |> paginate(Repo, params, @pagination)
  end

  @doc """
  Returns the list of requests.

  ## Examples

      iex> list_requests()
      [%Request{}, ...]

  """
  def list_requests do
    Repo.all(Request)
  end

  @doc """
  Gets a single request.

  Raises `Ecto.NoResultsError` if the Request does not exist.

  ## Examples

      iex> get_request!(123)
      %Request{}

      iex> get_request!(456)
      ** (Ecto.NoResultsError)

  """
  def get_request!(id), do: Repo.get!(Request, id)

  @doc """
  Gets a single request with it's centers.

  Raises `Ecto.NoResultsError` if the Request does not exist.
  """
  def get_request_with_centers!(id) do
    request = get_request!(id)
    Repo.preload(request, :centers)
  end

  @doc """
  Creates a request.

  ## Examples

      iex> create_request(%{field: value})
      {:ok, %Request{}}

      iex> create_request(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_request(attrs \\ %{}) do
    %Request{}
    |> Request.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a request.

  ## Examples

      iex> update_request(request, %{field: new_value})
      {:ok, %Request{}}

      iex> update_request(request, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_request(%Request{} = request, attrs) do
    request
    |> Request.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Request.

  ## Examples

      iex> delete_request(request)
      {:ok, %Request{}}

      iex> delete_request(request)
      {:error, %Ecto.Changeset{}}

  """
  def delete_request(%Request{} = request) do
    Repo.delete(request)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking request changes.

  ## Examples

      iex> change_request(request)
      %Ecto.Changeset{source: %Request{}}

  """
  def change_request(%Request{} = request) do
    Request.changeset(request, %{})
  end

  defp request_filter_config(:requests) do
    defconfig do
      text(:email)
      text(:key)
      text(:info)
      date(:completed_at)
    end
  end

  @doc """
  Gets a single center request.

  Raises `Ecto.NoResultsError` if the Request does not exist.

  ## Examples

      iex> get_center_request!(123)
      %CenterRequest{}

      iex> get_center_request!(456)
      ** (Ecto.NoResultsError)

  """
  def get_center_request!(id), do: Repo.get!(CenterRequest, id)

  @doc """
  Updates a center request.

  ## Examples

      iex> update_center_request(center_request, %{field: new_value})
      {:ok, %CenterRequest{}}

      iex> update_center_request(center_request, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_center_request(%CenterRequest{} = center_request, attrs) do
    center_request
    |> CenterRequest.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking request changes.

  ## Examples

      iex> change_centre_request(center_request)
      %Ecto.Changeset{source: %CenterRequest{}}

  """
  def change_center_request(%CenterRequest{} = center_request) do
    CenterRequest.changeset(center_request, %{})
  end

  @doc """
  Creates a private_data.
  """
  def create_private_data(center_request_id, file) do
    attrs = %{center_request_id: center_request_id, file: file}

    %PrivateData{center_request_id: center_request_id}
    |> PrivateData.changeset(attrs)
    |> Repo.insert()
  end
end
