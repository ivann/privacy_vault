defmodule PrivacyVault.Vault.CenterRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias PrivacyVault.Vault.Request
  alias PrivacyVault.Vault.PrivateData
  alias PrivacyVault.Centers.Center

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "center_requests" do
    field :completed_at, :utc_datetime

    belongs_to :center, Center
    belongs_to :request, Request

    has_many :private_data, PrivateData

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(center_request, attrs) do
    center_request
    |> cast(attrs, [:completed_at])
    |> assoc_constraint(:request)
    |> assoc_constraint(:center)
    |> validate_required(:completed_at)
  end
end
