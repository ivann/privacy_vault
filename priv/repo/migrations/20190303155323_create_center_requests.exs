defmodule PrivacyVault.Repo.Migrations.CreateCenterRequests do
  use Ecto.Migration

  def change do
    create table(:center_requests, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:completed_at, :utc_datetime)
      add(:center_id, references(:centers, on_delete: :nothing, type: :binary_id))
      add(:request_id, references(:requests, on_delete: :delete_all, type: :binary_id))

      timestamps(type: :utc_datetime)
    end

    create(index(:center_requests, [:center_id]))
    create(index(:center_requests, [:request_id]))
  end
end
