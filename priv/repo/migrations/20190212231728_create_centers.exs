defmodule PrivacyVault.Repo.Migrations.CreateCenters do
  use Ecto.Migration

  def change do
    create table(:centers, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :email, :string
      add :country, :string

      timestamps(type: :utc_datetime)
    end
  end
end
