defmodule PrivacyVault.Repo.Migrations.CreateRequests do
  use Ecto.Migration

  def change do
    create table(:requests, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:email, :string)
      add(:key, :string)
      add(:centers, :string)
      add(:info, :string)
      add(:completed_at, :utc_datetime)

      timestamps(type: :utc_datetime)
    end
  end
end
