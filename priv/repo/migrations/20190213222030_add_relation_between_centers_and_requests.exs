defmodule PrivacyVault.Repo.Migrations.AddRelationBetweenCentersAndRequests do
  use Ecto.Migration

  def change do
    alter table("requests") do
      remove :centers, :string
    end

    create table("centers_requests") do
      add :center_id, references(:centers, type: :uuid, on_delete: :delete_all)
      add :request_id, references(:requests, type: :uuid, on_delete: :delete_all)
    end
  end
end
