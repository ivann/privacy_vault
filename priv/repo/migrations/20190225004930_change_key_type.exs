defmodule PrivacyVault.Repo.Migrations.ChangeKeyType do
  use Ecto.Migration

  def change do
    alter table(:requests) do
      modify :key, :text, from: :string
    end
  end
end
