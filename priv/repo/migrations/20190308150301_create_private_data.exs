defmodule PrivacyVault.Repo.Migrations.CreatePrivateData do
  use Ecto.Migration

  def change do
    create table(:private_data, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :file, :string
      add :center_request_id, references(:center_requests, on_delete: :delete_all, type: :binary_id)

      timestamps()
    end

    create index(:private_data, [:center_request_id])
  end
end
