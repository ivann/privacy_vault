# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     PrivacyVault.Repo.insert!(%PrivacyVault.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

import PrivacyVault.Factory

# Centers without requests
insert_list(8, :center)

# Centers with uncompleted requests
build_list(4, :center)
|> Enum.map(fn center -> center |> with_requests() |> insert() end)

# Centers with completed requests
build_list(4, :center)
|> Enum.map(fn center ->
  center
  |> with_requests()
  |> (fn c -> %{c | requests: Enum.map(center.requests, &completed/1)} end).()
  |> insert()
end)

# Requests without centers
insert_list(8, :request)
