defmodule PrivacyVault.CenterRequestFactory do
  defmacro __using__(_opts) do
    quote do
      alias PrivacyVault.Vault.CenterRequest

      def center_request_factory do
        %CenterRequest{
          center: build(:center),
          request: build(:request),
          completed_at: nil
        }
      end

      def completed(%CenterRequest{} = center_request) do
        %{center_request | completed_at: Faker.Date.backward(365)}
      end
    end
  end
end
