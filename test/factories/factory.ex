defmodule PrivacyVault.Factory do
  use ExMachina.Ecto, repo: PrivacyVault.Repo

  use PrivacyVault.RequestFactory
  use PrivacyVault.CenterFactory
  use PrivacyVault.CenterRequestFactory
end
