defmodule PrivacyVault.RequestFactory do
  defmacro __using__(_opts) do
    quote do
      alias PrivacyVault.Vault.Request

      def request_factory do
        %Request{
          email: Faker.Internet.email(),
          info: Faker.Lorem.paragraph() |> String.slice(0..254),
          key: """
          -----BEGIN PGP PUBLIC KEY BLOCK-----
          Version: OpenPGP.js v4.4.7
          Comment: https://openpgpjs.org

          xsBNBFyVbMQBCACnBqx50NztrVkEKsEl1hKi0nLjtf7XQ6w5+Ov82yX5Q4yH
          H2hXl7IL/s+FCeC5LIO5NB/reJJdGxyRyX5rCoEXPuSERh8p1k1l+a6Yc7Bu
          VGfspOMJw88q91rA1kHCATytayJLXDFoI8xyLFy8SMEfwU1DciQEZnG6kN/W
          hQtttGqOQ5Dqp+hhIV+Ts73EIvuf7Zn6CSp09TIKhOcBgwvoQIFoA2I2DYEa
          RuGhFb0XJHnUuE0r/vF4Uh5KBQj+y0Xqv+Dn5ftq1qp0lqaLzfzLOLikMNTs
          RK3JbCYS/C+49UCnknXQhVgFsorCvHr12XSoj8Q55ZcyWkghPSohaca7ABEB
          AAHNCXNvbWV0aGluZ8LAdQQQAQgAHwUCXJVsxAYLCQcIAwIEFQgKAgMWAgEC
          GQECGwMCHgEACgkQWeM/ArRS0AGgYgf+If+lKJr2aTzAnHCtvhTeQS16mtTa
          3OwBV/v9Q863p/3sGOs/bAmgrPC5EXPjHWr0e9gcbqaE8A8Gdismnwh8ph9w
          GJU205IlSzfgncaE4aloWBMuU2zyAfuyh+kzOkUMB+g+Ep6N0zlPHu65D8N+
          sL3TZysmhhUzmDM0JRMvf7gsDPTJTrA95Bxzp1nULaXaU0IQcOAx9KVSKE1A
          mLtlxH7hozKXis31D+ouAkRD117fP5LJ01C67FsWKCf7rUWz1WekZH8cAU9S
          mkFKRflyQIaGeNvZL6jnhOhd44GALNBdO14Mqfozp/UD0UamLLa2jmmIEQ43
          T98Y1l/azD1J2s7ATQRclWzEAQgAxJVGAB4cxqdWwAgGeNBrSTt0nMeUsJ+G
          AR5Er8b0+V5KnejuKrBYT9ic+xrBnvG2uzQpdCEGMV1H44AhvkAflOrjemww
          sE2HSB89vX4Fjl8O16aNrkw2l+8C7pm4/EpLTKr64+1eB0aQRDZ8Y7DWzqSu
          3xtcwf87tJPexYejQZpe3pHYMN0h8E13bSmWxxLLfEiRSpgG7wACSY8mrh+a
          /DpxoIsoi/LwjCVJAxPftVzFwENHBtAw6+7eO48ZU1xXJRV/PuDdI+rSr2UH
          EUJCR0cMmNL1L69RiB3V2n8h0mtCwwKqakvdMz17NQ4JycgNTvitCXaNccZy
          I9szRnwZHwARAQABwsBfBBgBCAAJBQJclWzEAhsMAAoJEFnjPwK0UtAB7Q0I
          AIL1COeuLHj83G5XXGMT3cZhzB69wgAZTDiaSPuJdRw78E9nskABaPLsy6j4
          /dono/1SyxOTk4fT9M8cP0eZ6b2A1XV4Rze3cPvEoXTXsAqX5gwxl+VmQzLj
          hZN/nZ75SaWe+/kzUdF+gKSxfHj99qyo+HqFbV1KELhGUNLxldFZ93eZV9nD
          LROmHTVl8luKzrpECgcXa5kNwgQCssKX/hrxUFZV8NzNpLw+dL3osZRpK20D
          HmCMQ8t6cMO2SwnGeo9GYs3sq/Vq8DsDcSUhlvZShNhWDEn9sgvGHqsV0w+D
          yjnbr3JqUmsbumVxqLmHe0geAKUycdIRb4dYj7WKghs=
          =u2xI
          -----END PGP PUBLIC KEY BLOCK-----
          """,
          completed_at: nil,
          centers: []
        }
      end

      def with_centers(request, number \\ 2) do
        %{request | centers: build_list(number, :center)}
      end

      def completed(%Request{} = request) do
        %{request | completed_at: Faker.Date.backward(365)}
      end
    end
  end
end
