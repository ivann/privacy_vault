defmodule PrivacyVault.CenterFactory do
  defmacro __using__(_opts) do
    quote do
      def center_factory do
        %PrivacyVault.Centers.Center{
          name: Faker.Company.name(),
          email: Faker.Internet.email(),
          country: Faker.Address.country(),
          requests: []
        }
      end

      def with_requests(center, number \\ 2) do
        %{center | requests: build_list(number, :request)}
      end
    end
  end
end
