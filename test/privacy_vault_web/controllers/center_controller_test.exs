defmodule PrivacyVaultWeb.CenterControllerTest do
  use PrivacyVaultWeb.ConnCase

  alias PrivacyVault.Centers

  @create_attrs %{
    country: "some country",
    email: "some email",
    name: "some name"
  }

  def fixture(:center) do
    {:ok, center} = Centers.create_center(@create_attrs)
    center
  end

  describe "show" do
    setup [:create_center]

    test "a center", %{conn: conn, center: center} do
      conn = get(conn, Routes.center_center_path(conn, :show, center.id))
      assert html_response(conn, 200) =~ center.name
    end
  end

  defp create_center(_) do
    center = fixture(:center)
    {:ok, center: center}
  end
end
