defmodule PrivacyVaultWeb.Admin.RequestControllerTest do
  use PrivacyVaultWeb.ConnCase

  @create_attrs %{
    email: "some email",
    info: "some info",
    key: "some key"
  }
  @update_attrs %{
    email: "some updated email",
    info: "some updated info",
    key: "some updated key"
  }
  @invalid_attrs %{
    email: nil,
    info: nil,
    key: nil
  }

  def fixture(:request) do
    build(:request, @create_attrs) |> with_centers |> insert
  end

  def fixture(:centers) do
    insert_list(2, :center)
  end

  describe "index" do
    setup [:create_request]

    test "lists all requests", %{conn: conn} do
      conn = get(conn, Routes.admin_request_path(conn, :index))
      assert html_response(conn, 200) =~ "Requests"
    end
  end

  describe "new request" do
    setup [:create_centers]

    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_request_path(conn, :new))
      assert html_response(conn, 200) =~ "New Request"
    end
  end

  describe "create request" do
    setup [:create_centers]

    test "redirects to show when data is valid", %{conn: conn, centers: centers} do
      center_ids = Enum.map(centers, & &1.id)
      attrs = Enum.into(%{center_ids: center_ids}, @create_attrs)
      conn = post conn, Routes.admin_request_path(conn, :create), request: attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_request_path(conn, :show, id)

      conn = get(conn, Routes.admin_request_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Request Details"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, Routes.admin_request_path(conn, :create), request: @invalid_attrs
      assert html_response(conn, 200) =~ "New Request"
    end
  end

  describe "edit request" do
    setup [:create_request]

    test "renders form for editing chosen request", %{conn: conn, request: request} do
      conn = get(conn, Routes.admin_request_path(conn, :edit, request))
      assert html_response(conn, 200) =~ "Edit Request"
    end
  end

  describe "update request" do
    setup [:create_request, :create_centers]

    test "redirects when data is valid", %{conn: conn, request: request, centers: centers} do
      center_ids = Enum.map(centers, & &1.id)
      attrs = Enum.into(%{center_ids: center_ids}, @update_attrs)
      conn = put conn, Routes.admin_request_path(conn, :update, request), request: attrs
      assert redirected_to(conn) == Routes.admin_request_path(conn, :show, request)

      conn = get(conn, Routes.admin_request_path(conn, :show, request))
      assert html_response(conn, 200) =~ "some updated info"
    end

    test "renders errors when data is invalid", %{conn: conn, request: request} do
      conn = put conn, Routes.admin_request_path(conn, :update, request), request: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Request"
    end
  end

  describe "delete request" do
    setup [:create_request]

    test "deletes chosen request", %{conn: conn, request: request} do
      conn = delete(conn, Routes.admin_request_path(conn, :delete, request))
      assert redirected_to(conn) == Routes.admin_request_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.admin_request_path(conn, :show, request))
      end
    end
  end

  defp create_request(_) do
    request = fixture(:request)
    {:ok, request: request}
  end

  defp create_centers(_) do
    centers = fixture(:centers)
    {:ok, centers: centers}
  end
end
