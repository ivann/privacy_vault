defmodule PrivacyVaultWeb.Admin.CenterControllerTest do
  use PrivacyVaultWeb.ConnCase

  alias PrivacyVault.Centers

  @create_attrs %{country: "some country", email: "some email", name: "some name"}
  @update_attrs %{
    country: "some updated country",
    email: "some updated email",
    name: "some updated name"
  }
  @invalid_attrs %{country: nil, email: nil, name: nil}

  def fixture(:center) do
    {:ok, center} = Centers.create_center(@create_attrs)
    center
  end

  describe "index" do
    test "lists all centers", %{conn: conn} do
      conn = get(conn, Routes.admin_center_path(conn, :index))
      assert html_response(conn, 200) =~ "Centers"
    end
  end

  describe "new center" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_center_path(conn, :new))
      assert html_response(conn, 200) =~ "New Center"
    end
  end

  describe "create center" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, Routes.admin_center_path(conn, :create), center: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_center_path(conn, :show, id)

      conn = get(conn, Routes.admin_center_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Center Details"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, Routes.admin_center_path(conn, :create), center: @invalid_attrs
      assert html_response(conn, 200) =~ "New Center"
    end
  end

  describe "edit center" do
    setup [:create_center]

    test "renders form for editing chosen center", %{conn: conn, center: center} do
      conn = get(conn, Routes.admin_center_path(conn, :edit, center))
      assert html_response(conn, 200) =~ "Edit Center"
    end
  end

  describe "update center" do
    setup [:create_center]

    test "redirects when data is valid", %{conn: conn, center: center} do
      conn = put conn, Routes.admin_center_path(conn, :update, center), center: @update_attrs
      assert redirected_to(conn) == Routes.admin_center_path(conn, :show, center)

      conn = get(conn, Routes.admin_center_path(conn, :show, center))
      assert html_response(conn, 200) =~ "some updated country"
    end

    test "renders errors when data is invalid", %{conn: conn, center: center} do
      conn = put conn, Routes.admin_center_path(conn, :update, center), center: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Center"
    end
  end

  describe "delete center" do
    setup [:create_center]

    test "deletes chosen center", %{conn: conn, center: center} do
      conn = delete(conn, Routes.admin_center_path(conn, :delete, center))
      assert redirected_to(conn) == Routes.admin_center_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.admin_center_path(conn, :show, center))
      end
    end
  end

  defp create_center(_) do
    center = fixture(:center)
    {:ok, center: center}
  end
end
