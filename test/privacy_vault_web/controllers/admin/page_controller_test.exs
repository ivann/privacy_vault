defmodule PrivacyVaultWeb.Admin.PageControllerTest do
  use PrivacyVaultWeb.ConnCase

  describe "index" do
    test "show admin main page", %{conn: conn} do
      conn = get(conn, Routes.admin_page_path(conn, :index))
      assert html_response(conn, 200) =~ "Admin"
    end
  end
end
