defmodule PrivacyVaultWeb.RequestControllerTest do
  use PrivacyVaultWeb.ConnCase

  @create_attrs %{
    completed_at: ~N[2010-04-17 14:00:00],
    email: "some email",
    info: "some info",
    key: "some key"
  }
  @invalid_attrs %{
    completed_at: nil,
    email: nil,
    info: nil,
    key: nil
  }

  def fixture(:centers) do
    insert_list(2, :center)
  end

  describe "new request" do
    setup [:create_centers]

    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.request_path(conn, :new))
      assert html_response(conn, 200) =~ "New Request"
    end
  end

  describe "create request" do
    setup [:create_centers]

    test "redirects to show when data is valid", %{conn: conn, centers: centers} do
      center_ids = Enum.map(centers, & &1.id)
      attrs = Enum.into(%{center_ids: center_ids}, @create_attrs)
      conn = post(conn, Routes.request_path(conn, :create), request: attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.request_path(conn, :show, id)

      conn = get(conn, Routes.request_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Request"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.request_path(conn, :create), request: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Request"
    end
  end

  defp create_centers(_) do
    centers = fixture(:centers)
    {:ok, centers: centers}
  end
end
