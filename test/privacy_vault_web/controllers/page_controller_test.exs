defmodule PrivacyVaultWeb.PageControllerTest do
  use PrivacyVaultWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Privacy Vault"
  end
end
