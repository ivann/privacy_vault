defmodule PrivacyVault.CentersTest do
  use PrivacyVault.DataCase

  alias PrivacyVault.Centers

  describe "centers" do
    alias PrivacyVault.Centers.Center

    @valid_attrs %{
      country: "some country",
      email: "some email",
      name: "some name"
    }
    @update_attrs %{
      country: "some updated country",
      email: "some updated email",
      name: "some updated name"
    }
    @invalid_attrs %{
      country: nil,
      email: nil,
      name: nil
    }

    def center_fixture(attrs \\ %{}) do
      {:ok, center} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Centers.create_center()

      center
    end

    test "paginate_centers/1 returns paginated list of centers" do
      for _ <- 1..20 do
        center_fixture()
      end

      {:ok, %{centers: centers} = page} = Centers.paginate_centers(%{})

      assert length(centers) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_centers/0 returns all centers" do
      center = center_fixture()
      assert Centers.list_centers() == [center]
    end

    test "get_center!/1 returns the center with given id" do
      center = center_fixture()
      assert Centers.get_center!(center.id) == center
    end

    test "create_center/1 with valid data creates a center" do
      assert {:ok, %Center{} = center} = Centers.create_center(@valid_attrs)
      assert center.country == "some country"
      assert center.email == "some email"
      assert center.name == "some name"
    end

    test "create_center/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Centers.create_center(@invalid_attrs)
    end

    test "update_center/2 with valid data updates the center" do
      center = center_fixture()
      assert {:ok, center} = Centers.update_center(center, @update_attrs)
      assert %Center{} = center
      assert center.country == "some updated country"
      assert center.email == "some updated email"
      assert center.name == "some updated name"
    end

    test "update_center/2 with invalid data returns error changeset" do
      center = center_fixture()
      assert {:error, %Ecto.Changeset{}} = Centers.update_center(center, @invalid_attrs)
      assert center == Centers.get_center!(center.id)
    end

    test "delete_center/1 deletes the center" do
      center = center_fixture()
      assert {:ok, %Center{}} = Centers.delete_center(center)
      assert_raise Ecto.NoResultsError, fn -> Centers.get_center!(center.id) end
    end

    test "change_center/1 returns a center changeset" do
      center = center_fixture()
      assert %Ecto.Changeset{} = Centers.change_center(center)
    end
  end
end
