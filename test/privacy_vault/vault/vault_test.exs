defmodule PrivacyVault.VaultTest do
  use PrivacyVault.DataCase

  alias PrivacyVault.Vault

  describe "requests" do
    alias PrivacyVault.Vault.Request

    @valid_attrs %{
      email: "some email",
      info: "some info",
      key: "some key"
    }
    @update_attrs %{
      email: "some updated email",
      info: "some updated info",
      key: "some updated key"
    }
    @invalid_attrs %{
      email: nil,
      info: nil,
      key: nil
    }

    def request_fixture(attrs \\ %{}) do
      {:ok, request} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Vault.create_request()

      request
    end

    test "paginate_requests/1 returns paginated list of requests" do
      for _ <- 1..20 do
        request_fixture()
      end

      {:ok, %{requests: requests} = page} = Vault.paginate_requests(%{})

      assert length(requests) == 15
      assert page.page_number == 1
      assert page.page_size == 15
      assert page.total_pages == 2
      assert page.total_entries == 20
      assert page.distance == 5
      assert page.sort_field == "inserted_at"
      assert page.sort_direction == "desc"
    end

    test "list_requests/0 returns all requests" do
      request = request_fixture()
      assert Enum.map(Vault.list_requests(), & &1.id) == [request.id]
    end

    test "get_request!/1 returns the request with given id" do
      request = request_fixture()
      assert Vault.get_request!(request.id).id == request.id
    end

    test "create_request/1 with valid data creates a request" do
      assert {:ok, %Request{} = request} = Vault.create_request(@valid_attrs)
      assert request.email == "some email"
      assert request.info == "some info"
      assert request.key == "some key"
    end

    test "create_request/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Vault.create_request(@invalid_attrs)
    end

    test "create_request/1 with existing center ids create a request with centers associations" do
      center_ids = Enum.map(insert_list(2, :center), & &1.id)

      assert {:ok, %Request{} = request} =
               Vault.create_request(Enum.into(%{center_ids: center_ids}, @valid_attrs))

      assert Enum.map(Vault.get_request_with_centers!(request.id).centers, & &1.id) == center_ids
    end

    test "create_request/1 with non existing center ids raise an exception" do
      assert_raise Ecto.NoResultsError, fn ->
        Vault.create_request(
          Enum.into(%{center_ids: ["00000000-0000-0000-0000-000000000000"]}, @valid_attrs)
        )
      end
    end

    test "update_request/2 with valid data updates the request" do
      request = request_fixture()
      assert {:ok, request} = Vault.update_request(request, @update_attrs)
      assert %Request{} = request
      assert request.email == "some updated email"
      assert request.info == "some updated info"
      assert request.key == "some updated key"
    end

    test "update_request/2 with invalid data returns error changeset" do
      request = request_fixture()
      assert {:error, %Ecto.Changeset{}} = Vault.update_request(request, @invalid_attrs)
      assert request.id == Vault.get_request!(request.id).id
    end

    test "update_request/2 with existing center ids add centers associations" do
      request = request_fixture()
      center_ids = Enum.map(insert_list(2, :center), & &1.id)

      assert {:ok, %Request{} = request} =
               Vault.update_request(request, Enum.into(%{center_ids: center_ids}, @update_attrs))

      assert Enum.map(Vault.get_request_with_centers!(request.id).centers, & &1.id) == center_ids
    end

    test "update_request/2 with non existing center ids raise an exception" do
      request = request_fixture()

      assert_raise Ecto.NoResultsError, fn ->
        Vault.update_request(
          request,
          Enum.into(%{center_ids: ["00000000-0000-0000-0000-000000000000"]}, @update_attrs)
        )
      end
    end

    test "delete_request/1 deletes the request" do
      request = request_fixture()
      assert {:ok, %Request{}} = Vault.delete_request(request)
      assert_raise Ecto.NoResultsError, fn -> Vault.get_request!(request.id) end
    end

    test "change_request/1 returns a request changeset" do
      request = request_fixture()
      assert %Ecto.Changeset{} = Vault.change_request(request)
    end
  end

  describe "center_requests" do
    alias PrivacyVault.Vault.CenterRequest

    @update_attrs %{
      completed_at: "2019-01-01T00:00:00+00:00"
    }
    @invalid_attrs %{
      completed_at: "not a date"
    }

    def center_request_fixture(attrs \\ %{}) do
      insert(:center_request, attrs)
    end

    test "get_center_request!/1 returns the center_request with given id" do
      center_request = center_request_fixture()
      assert Vault.get_center_request!(center_request.id).id == center_request.id
    end

    test "update_center_request/2 with valid data updates the center_request" do
      center_request = center_request_fixture()
      assert {:ok, center_request} = Vault.update_center_request(center_request, @update_attrs)
      assert %CenterRequest{} = center_request
      {:ok, datetime, 0} = DateTime.from_iso8601("2019-01-01T00:00:00Z")
      assert center_request.completed_at == datetime
    end

    test "update_center_request/2 with invalid data returns error changeset" do
      center_request = center_request_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Vault.update_center_request(center_request, @invalid_attrs)

      assert center_request.id == Vault.get_center_request!(center_request.id).id
    end
  end
end
