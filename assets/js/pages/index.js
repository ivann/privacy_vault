export default {
    el: '#open-request',
    data() {
        return {
            uuid: ''
        }
    },
    methods: {
        open: function () {
            window.location.href = "/request/" + this.uuid
        }
    }
}
