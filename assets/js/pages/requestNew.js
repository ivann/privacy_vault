import openpgp from "openpgp"

export default {
    el: '#request-form',
    data: {
        publicKey: '',
    },
    computed: {
        disableSubmit: function () {
            return '' === this.publicKey
        },
        keyStatus: function() {
            if ('' !== this.publicKey) {
                return 'Generating key: done.'
            } else {
                return 'Generating key...'
            }
        }
    },
    mounted: function () {
        var privateKey = localStorage.getItem('privateKey')
        var publicKey = localStorage.getItem('publicKey')
        if (!(publicKey && privateKey)) {
            this.generateKeypair()
        } else {
            this.publicKey = publicKey
        }
    },
    methods: {
        generateKeypair: function() {
            self = this
            const options = {
              userIds: [{ name: "my privacy vault key" }],
              numBits: 2048
            }
            openpgp.generateKey(options).then(function(key) {
                localStorage.setItem('privateKey', key.privateKeyArmored)
                localStorage.setItem('publicKey', key.publicKeyArmored)
                self.publicKey = key.publicKeyArmored
            })
        },
    }
}
