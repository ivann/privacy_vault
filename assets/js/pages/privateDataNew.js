import Vue from 'vue'
import * as openpgp from 'openpgp'

Vue.component('private-file', {
  props: ['index', 'file'],
  template: `
    <div>
        <input
            :id="'center_request_private_data_' + index + '_file'"
            :name="'center_request[private_data][' + index + '][file]'"
            @change="$emit('file-change', index, $event.target.files[0])"
            type="file"
        />
        <span>{{ file.state }}</span>
    </div>
    `
})

Vue.component('private-data-form', {
  data: function () {
    return {
      files: [],
      ready: true,
      complete: false
    }
  },
  mounted: function () {
    this.addPrivateFile()
  },
  methods: {
    addPrivateFile: function () {
      const file = {
        content: null,
        state: ''
      }
      this.files.push(file)
    },
    removePrivateFile: function () {
      if (this.files.length < 2) {
        return
      }
      this.files.pop()
    },
    fileChange: function (index, file) {
      this.files[index].content = file
    },
    submit: function () {
      this.ready = false
      Promise.all(
        this.files.filter(file => file.content !== null).map(file => {
          file.state = 'encrypting'
          return this.readFile(file.content)
            .then(this.encryptFile)
            .then(encrypted => {
              file.state = 'uploading'
              return this.uploadFile(encrypted)
            })
            .then(() => {
              file.state = 'done'
            })
        })
      )
        .then(() => {
          this.ready = true
          if (this.complete) {
            this.completeRequest()
          }
        })
        .catch(error => console.log('Error: ', error))
    },
    readFile: function (file) {
      return new Promise((resolve, reject) => {
        const fileReader = new FileReader()
        fileReader.addEventListener('loadend', (e) => {
          if (e.target.readyState === 2) {
            resolve(e.target.result)
          } else {
            reject(e.target.error)
          }
        })
        fileReader.readAsArrayBuffer(file)
      })
    },
    encryptFile: function (file) {
      return new Promise((resolve, reject) => {
        const encrypt = async () => {
          const options = {
            message: openpgp.message.fromBinary(new Uint8Array(file)),
            publicKeys: (await openpgp.key.readArmored(window.publicKey)).keys,
            armor: false,
            compression: openpgp.enums.compression.zlib
          }
          openpgp.encrypt(options).then(ciphetext => {
            resolve(
              new Blob([ciphetext.message.packets.write()], { type: 'application/octet-stream' })
            )
          })
        }
        encrypt()
      })
    },
    completeRequest: function () {
      return this.post(window.location.pathname, {
        'center_request[complete]': true
      })
    },
    uploadFile: function (file) {
      return this.post(window.location.pathname + '/upload', {
        'file': file
      })
    },
    post: function (url, data) {
      const formData = new FormData()
      for (const [key, value] of Object.entries(data)) {
        formData.append(key, value)
      }
      formData.append('_csrf_token', window.csrfToken)
      return fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        body: formData
      })
    }
  },
  template: `
    <div>
      <div class="float-right">
        <div class="form-group">
          <button
            class="button-outline"
            v-on:click.prevent='removePrivateFile'
            :disabled='files.length < 2'
          >
            -
          </button>
          <button
            class="button-outline"
            v-on:click.prevent='addPrivateFile'
          >
            +
          </button>
        </div>
        <div class="form-group">
          <input type="checkbox" id="request-complete" v-model="complete">
          <label class="label-inline" for="request-complete">Complete</label>
        </div>
        <div class="form-group">
          <button
            v-on:click.prevent='submit'
            :disabled='!ready'
          >
            Submit
          </button>
        </div>
      </div>
      <div class="form-group">
        <private-file v-for='(f, i) in files' v-on:file-change='fileChange' :key='i' :index='i' :file='f' />
      </div>
    </div>
  `
})

export default {
  el: '#private-data-form',
  template: `<private-data-form />`
}
