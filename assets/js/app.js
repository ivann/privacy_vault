// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.sass"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

import Vue from "vue"

import requestNew from "./pages/requestNew"
import index from "./pages/index"
import privateDataNew from "./pages/privateDataNew"

var routes = {
    "/": index,
    "/request/new": requestNew,
    "/center/requests/.*": privateDataNew
}

function route(path, routes) {
    for (const [route, fun] of Object.entries(routes)) {
        if (path.match("^" + route + "$")) {
            new Vue(fun)
        }
    }
}

route(window.location.pathname, routes)
